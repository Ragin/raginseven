package org.ragin.custom;

import java.io.IOException;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import org.apache.log4j.*;

/**
 * example of a custom tag class. not used in this projecdt
 * @author mmueller9
 */
public class RTagHello extends TagSupport {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
  private static final long serialVersionUID = 1L;
  private PageContext pageContext; 
  private Tag parentTag; 
  
  private String surname;
  private String prename; 
  private String cityOfBirth; 

    public String getCityOfBirth() {
        return cityOfBirth;
    }

    public void setCityOfBirth(String cityOfBirth) {
        this.cityOfBirth = cityOfBirth;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }
    
   
 
   @Override
   public void setPageContext(PageContext pageContext) {
       this.pageContext = pageContext; 
   } 
  
   @Override
   public void setParent(Tag parentTag) {
       this.parentTag = parentTag; 
   }
   
   @Override
   public Tag getParent() {
       return this.parentTag; 
   }
    
   
  @Override
  public int doStartTag() throws JspException {
    ServletRequest request = pageContext.getRequest();
    
    JspWriter jspWriter = pageContext.getOut();
    try {
      if((surname == null) || (prename==null)){
          jspWriter.println("guest" );
      } else {
          jspWriter.println(prename + " " + surname );
      }
      if (cityOfBirth != null) {
          jspWriter.println(" from " + cityOfBirth );
      }
      if (request.getParameter("age")!= null) {
           jspWriter.println("<br/>Your age was submittet using a request param: " + request.getParameter("age")
            + "<br/>Name and city were set using jsp:attributes in file tests.tag");
      } else {
           jspWriter.println("<br/>Your age was not submitted using a request param. " );
           
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return SKIP_BODY;
  }
 
 
}