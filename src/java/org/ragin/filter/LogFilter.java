/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.filter;
// Import required java libraries

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import org.apache.log4j.*;

/*
    <filter>
        <display-name>LogFilter</display-name>
        <filter-name>LogFilter</filter-name>
        <filter-class>org.ragin.filter.LogFilter</filter-class>
        <init-param>
            <param-name>test-filter-param</param-name>
            <param-value>Filter Initialization Paramter</param-value>
        </init-param>
    </filter>
*/

@WebFilter(urlPatterns = {"/*"},
        description = "does some logging",
        dispatcherTypes = DispatcherType.REQUEST,
        displayName = "LogFilter",
        filterName =  "LogFilter",
        //servletNames = {"EvaluateController","FrontController"},
        asyncSupported = true,
        initParams = {
            @WebInitParam(name = "test-filter-param", value = "Filter Initialization Paramter", description = "not used")
        }
)



// Implements Filter class
public class LogFilter implements Filter {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //System.out.println("Filter initialized...");
        logger.log(Level.INFO, "LogFilter initialized... ");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain filterChain) throws IOException, ServletException {
        //System.out.println("Filter executing...");
        //logger.log(Level.DEBUG, "log filtering... " );
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        System.out.println("Filter Destroyed..");
    }
}
