/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.filter;

import java.io.IOException;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.*;


/*
    <filter>
        <display-name>LoginFilter</display-name>
        <filter-name>LoginFilter</filter-name>
        <filter-class>org.ragin.filter.LoginFilter</filter-class>
        <init-param>
            <param-name>login-filter-param</param-name>
            <param-value>Login Filter Initialization Paramter</param-value>
        </init-param>
    </filter>
*/ 

@WebFilter(urlPatterns = {"/examNew"},
        description = "do exam only after login",
        dispatcherTypes = DispatcherType.REQUEST,
        displayName = "LoginFilter",
        filterName =  "LoginFilter",
        //servletNames = {"EvaluateController","FrontController"},
        asyncSupported = true,
        initParams = {
            @WebInitParam(name = "exampleParam2", value = "jpg,jpeg,gif,png", 
                    description = "not used")
        }
)

/**
 * @author Stuart Douglas
 */
public class LoginFilter implements Filter {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        logger.log(Level.INFO, "LoginFilter initialized... ");
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        //logger.log(Level.DEBUG, "login filtering..");

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession(false);
        // loginform pattern
        String loginURI = req.getContextPath() + "/lform";

        // test if login.jsp has set this attribute into session
        boolean loggedIn = session != null && session.getAttribute("userid") != null;
        boolean loginRequest = req.getRequestURI().equals(loginURI);
        boolean isLoginFormItself = ( 
                req.getRequestURI().contains("/lform") // login form
                || req.getRequestURI().contains("/login") // server log
                || req.getRequestURI().contains("/registration") // server log
                || req.getRequestURI().contains("/reg") // registration form
                || req.getRequestURI().contains("/index") // registration form
                || req.getRequestURI().contains("/appLog") // app log
                || req.getRequestURI().contains("/serverLog") // server log
                )
                ;

        // make sure, we show not the loginform itself. this must be reachable
        // any time!
        if (isLoginFormItself) {
            //logger.log(Level.INFO, "this is user login form ");
            // dont forget to continue here. otherwise you cannot login
            chain.doFilter(request, response);
        } else
        if (loggedIn) {
            //logger.log(Level.INFO, "user is logged in... ");
            chain.doFilter(request, response);
        } else {
            //logger.log(Level.INFO, "userid NOT in session. redirecting to " + loginURI);
            resp.sendRedirect(loginURI);
            return; 
            //chain.doFilter(request, response);
        }

    }

    @Override
    public void destroy() {

    }
}
