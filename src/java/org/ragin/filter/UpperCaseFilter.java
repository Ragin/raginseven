/*
 * Simple response wrapper that utilizes a local CharArrayWriter for output
 * note that this is not working correctly , as is also converts URLs to uppercase
 * and therefore case-sensitive urls are not found!!
 */
package org.ragin.filter;

import org.apache.log4j.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;



/**
 * response wrapper class
 *
 * @author mmueller9
 */
class MyResponse extends HttpServletResponseWrapper  {

    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private PrintWriter out;
    private StringWriter sw;

    public MyResponse(HttpServletResponse response) {
        super(response);
        logger.log(Level.INFO, "createt ResponseWrapper");
        sw = new StringWriter();
        out = new PrintWriter(sw);
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        return out;
    }

    String getText() {
        out.flush();
        return sw.toString();
    }
}

/**
 * example how to use a servletresponseWrapper filter
 *
 * @author mmueller9
 */
@WebFilter(urlPatterns = {"/text"}, // apply only to text requests!
        description = "converts text to uppercase",
        dispatcherTypes = DispatcherType.REQUEST,
        displayName = "UpperCaseFilter",
        asyncSupported = true,
        initParams = {
            @WebInitParam(name = "exampleParam2", value = "jpg,jpeg,gif,png", description = "not used")
        }
)
/*
    <filter>
        <filter-name>UpperCaseFilter</filter-name>
        <filter-class>org.ragin.filter.UpperCaseFilter</filter-class>
    </filter>
 */
public class UpperCaseFilter implements Filter {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    //-------------------------------------------------------------------------
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletResponse realResponse = (HttpServletResponse) response;
        // create my own wrapper class
        MyResponse myResponse = new MyResponse(realResponse);
        // put my own response into the filter chain
        chain.doFilter(request, myResponse);
        // get the servlet-modified outstream
        String text = myResponse.getText();
        //modify the outstream
        text = text.toUpperCase();
        logger.log(Level.INFO, "converted text to uppercase... ");
        // put the modified outstream into the real response
        response.getWriter().print(text);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }

}
