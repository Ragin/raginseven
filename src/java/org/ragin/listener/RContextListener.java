/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.listener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import org.apache.log4j.*;
import org.ragin.controller.UploadServlet;
import org.ragin.model.Frage;

/**
 * Web application lifecycle listener.
 *
 * @author mmueller9
 */
@WebListener
public class RContextListener implements ServletContextListener {

    private final Logger logger = Logger.getLogger(this.getClass().getName());
    @Resource(name = "jdbc/RaginUsers") // resource injection from container!
    private DataSource dataSource;

    @PostConstruct
    private void nachInjection() {
        System.out.println("DataSource nach Injection: " + dataSource);
    }

    @PreDestroy
    private void nachDestroy() {
        System.out.println("DataSource nach destroy :" + dataSource);
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            ServletContext context = sce.getServletContext();
            context.setInitParameter("para1", "Hund");
            context.declareRoles("developer");

            // logging should be the first go get all other docs running
            String log4jConfigFile = context.getInitParameter("log4j-config-location");
            context.setAttribute("log4jConfigFile", log4jConfigFile);
            String fullPath = context.getRealPath(log4jConfigFile);
            System.out.println("============= LOG4J : " + fullPath + " ============");

            

            // create a new attribute in context.
            // initialize log4j here
            String log4jConfiguration;
            // set systemvariable for insertion into log4j.properties first:
            System.setProperty("application-logfile-path", context.getInitParameter("pathAppLog"));
            try {
                log4jConfiguration = new Scanner(new File(fullPath)).useDelimiter("\\Z").next();
                //logger.log(Level.INFO, log4jConfiguration);
                context.setAttribute("log4jConfiguration", log4jConfiguration);
                //logger.log(Level.INFO, "log4j file dynamically set to system properties: " + context.getInitParameter("pathAppLog"));
            } catch (FileNotFoundException ex) {
                java.util.logging.Logger.getLogger(RContextListener.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            PropertyConfigurator.configure(fullPath);


            // clear log file
            if (context.getInitParameter("clearAppLogOnContextLoad").equalsIgnoreCase("true")) {
                try {
                    FileOutputStream writer = null;
                    writer = new FileOutputStream(System.getProperty("application-logfile-path"));
                    writer.write(("*** Logfile cleared ***\r\n").getBytes());
                    writer.close();
                    logger.log(Level.INFO, "Logfile cleared on application startup.  ");
                } catch (FileNotFoundException ex) {
                    java.util.logging.Logger.getLogger(RContextListener.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
            } else {
                logger.log(Level.INFO, "Logfile NOT cleared on application startup.  ");
            }
            for (int i = 0; i < 4; i++) {
                logger.log(Level.INFO, " ");
            }
            for (int i = 0; i < 4; i++) {
                logger.log(Level.INFO, "============= INIT============");
            }
            logger.log(Level.INFO, "============= LOG4J ============");
            logger.log(Level.INFO, "---> Log4J config:  " + fullPath);
            // more:
            // context.addFilter(filterName, filterClass)
            // context.addListener(listenerClass);
            logger.log(Level.INFO, "============= PROGRAMMATICAL SERVLET REGISTRATION ============");
            // dynamic / programmatical registration example
            ServletRegistration.Dynamic reg;
            // 1. Variante
            // Servlet Name MUST NOT BE IDENTICAL with one used in web.xml !!
            reg = context.addServlet("UploadServlet2", "org.ragin.controller.UploadServlet");
            reg.addMapping("/dateienHochladen");
            reg.addMapping("/uploadFiles");
            reg.setInitParameter("param2", "bla");
            reg.setInitParameter("param399", "fasel");
            // 2. Variante
            reg = context.addServlet("HochladeServlet", org.ragin.controller.UploadServlet.class);
            try {
                // reg = context.addServlet("HochladeServletX", Class.forName("org.ragin.controller.UploadServlet"));
                context.addFilter("asdfö", context.createFilter(org.ragin.filter.LogFilter.class));
            } catch (ServletException ex) {
                java.util.logging.Logger.getLogger(RContextListener.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            reg.addMapping("/uploadFilesA");
            reg.setInitParameter("param23", "bla");
            reg.setInitParameter("param3499", "fasel");
            /*
             */

            // 3. Variante
            Servlet servlet = new UploadServlet();
            reg = context.addServlet("UploadServlet3", servlet);
            reg.addMapping("/uploadFiles2", "/uploadFiles3");
            reg.setInitParameter("param42", "bla");
            reg.setInitParameter("param499", "fasel");

            // 4. Variante
            try {
                Servlet servlet2 = context.createServlet(org.ragin.controller.UploadServlet.class);
                reg = context.addServlet("UploadServlet4", servlet2);
                reg.addMapping("/uploadFiles4", "/uploadFiles5");
            } catch (ServletException ex) {
                java.util.logging.Logger.getLogger(RContextListener.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }

            //FilterRegistration.Dynamic freg  ;
            //freg.addMappingForUrlPatterns(DispatcherType.REQUEST, true, "/bla/*");
            logger.log(Level.INFO, "============= JNDI ============");
            // data source , JNDI example
            // this will be obsolute using the @Resource Annotation !
            /*
            try {
            Context initContext = new InitialContext();
            // lookup liefert Object, also casten
            Context envContext = (Context) initContext.lookup("java:com/env");
            dataSource = (DataSource) envContext.lookup("jdbc/RaginUsers");  // prefix jdbc/ ist für databaseobjekte notwendig!
            logger.log(Level.INFO, "JNDI:  dataSource object found! " );
            } catch (NamingException ex) {
            logger.log(Level.INFO, "JNDI:  dataSource object NOT found! " );
            }
             */
            try (Connection connection = dataSource.getConnection()) {
                logger.log(Level.INFO, "---> JNDI:  dataSourced object found. ");
                //logger.log(Level.INFO, "JNDI:  connectio: " + connection);
                boolean reachable = connection.isValid(5);// 10 sec
                logger.log(Level.INFO, "---> connection is valid(5 sec): " + reachable);

            } catch (SQLException e) {
                logger.log(Level.INFO, "---> Fehler:  " + e.getMessage());
            }
            logger.log(Level.INFO, "============= CONTAINER INSPECTION ============");
            // inspect the container
            String serverInfo = context.getServerInfo();
            int servletAPIMinorVersion = context.getMinorVersion();
            int servletAPIMajorVersion = context.getMajorVersion();
            context.setAttribute("serverInfo", serverInfo);
            context.setAttribute("servletAPIMinorVersion", servletAPIMinorVersion);
            context.setAttribute("servletAPIMajorVersion", servletAPIMajorVersion);

            logger.log(Level.INFO, "============= DATABASE ============");
            initDatabase(sce);

            // init exam
            logger.log(Level.INFO, "============= QUESTIONS CSV ============");
            loadExamQuestionsFromCSV(sce);
            logger.log(Level.INFO, "============= INIT DONE ============");

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(RContextListener.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

    }

    /**
     * check existance of DB or create new
     */
    private void initDatabase(ServletContextEvent sce) {
        // init member database
        DatabaseMetaData dbmd;
        Statement stmnt = null;
        String JDBC_URL = sce.getServletContext().getInitParameter("jdbcURL");
        try {
            // Try to connect to the DB:
            Connection dbConnection = null;
            dbConnection = DriverManager.getConnection(JDBC_URL);
            logger.log(Level.INFO, "---> Using JDBC properties from deployment descriptor: Database EXISTS: " + JDBC_URL);
            // Create the table addresses if it doesn't exist:
            // not implemented
            /*
            if (!tableExists(dbConnection, "members")) {
                logger.log(Level.INFO, "Creating table members...");
                createMyTable(sce);
            } else {
                logger.log(Level.INFO, "table members exists.");
            }
             */
        } catch (Exception e) {
            logger.log(Level.INFO, "---> Can't create table: " + e.getMessage());

        }
    }

    // Derby doesn't support the standard SQL views.  To see if a table
    // exists you normally query the right view and see if any rows are
    // returned (none if no such table, one if table exists).  Derby
    // does support a non-standard set of views which are complicated,
    // but standard JDBC supports a DatabaseMetaData.getTables method.
    // That returns a ResultSet but not one where you can easily count
    // rows by "rs.last(); int numRows = rs.getRow()".  Hence the loop.
    private static boolean tableExists(Connection con, String table) {
        int numRows = 0;
        try {
            DatabaseMetaData dbmd = con.getMetaData();
            // Note the args to getTables are case-sensitive!
            ResultSet rs = dbmd.getTables(null, null, table.toUpperCase(), null);
            while (rs.next()) {
                ++numRows;
            }
        } catch (SQLException e) {
            String theError = e.getSQLState();
            System.out.println("Can't query DB metadata: " + theError);
            System.exit(1);
        }
        return numRows > 0;
    }

    private void loadExamQuestionsFromCSV(ServletContextEvent sce) {

        String CSVPath = sce.getServletContext().getRealPath("/WEB-INF/data/questions.csv");
        sce.getServletContext().setAttribute("CSVPath", CSVPath);
        logger.log(Level.INFO, "---> loaded path of CSV file into attibute CSVPath");

        Frage starter = new Frage();
        ArrayList<Frage> questions = starter.initFragenkatalogFromCSV(CSVPath);
        sce.getServletContext().setAttribute("questions", questions);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.log(Level.INFO, "--->  ");
        logger.log(Level.INFO, "--->  ");
        logger.log(Level.INFO, "---> context destroyed");
    }

    /**
     *
     */
    private void createMyTable(ServletContextEvent sce) throws FileNotFoundException {
        logger.log(Level.INFO, "---> creating new database");
        String query = "";
        String createTableStmt = "";

        String SQLPath = sce.getServletContext().getRealPath("/WEB-INF/data/derby-create.sql");
        Scanner scanner = new Scanner(new File(SQLPath));
        while (scanner.hasNextLine()) {
            query = new StringBuilder().append(query).append(scanner.nextLine()).toString();
        }
        scanner.close();

        logger.log(Level.INFO, "---> execute: " + query);
        // Create MySql Connection

        String JDBC_URL = sce.getServletContext().getInitParameter("jdbcURL");
        try {
            // Try to connect to the DB:
            Connection dbConnection = null;
            dbConnection = DriverManager.getConnection(JDBC_URL);
            logger.log(Level.INFO, "---> Database EXISTS: " + JDBC_URL);
            logger.log(Level.INFO, "---> xx!");
            Statement stmt = dbConnection.createStatement();
            logger.log(Level.INFO, "---> x!");
            stmt.execute("CREATE TABLE members (\n"
                    + "  id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),\n"
                    + "  first_name varchar(45) NOT NULL,\n"
                    + "  last_name varchar(45) NOT NULL,\n"
                    + "  email varchar(45) NOT NULL,\n"
                    + "  uname varchar(45) NOT NULL,\n"
                    + "  pass varchar(45) NOT NULL,\n"
                    + "  regdate date,\n"
                    + "  PRIMARY KEY  (id)\n"
                    + ")");
            logger.log(Level.INFO, "---> executed!");
        } catch (SQLException ex) {
            logger.log(Level.ERROR, "---> failed!");
            java.util.logging.Logger.getLogger(RContextListener.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

    }
}
