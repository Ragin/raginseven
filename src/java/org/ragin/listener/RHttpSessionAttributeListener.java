/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.apache.log4j.*;
 
/**
 * Web application lifecycle listener.
 *
 * @author Connacht
 */
@WebListener
public class RHttpSessionAttributeListener implements HttpSessionListener {
private final Logger logger = Logger.getLogger(this.getClass().getName());
    @Override
    public void sessionCreated(HttpSessionEvent se) {
       logger.log(Level.INFO, "sessionCreated");
       
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
       logger.log(Level.INFO, "sessionDestroyed");
    }
}
