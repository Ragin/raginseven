package org.ragin.listener;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

@WebListener
/** 
 * dynamisches Registrieren von Servlets durch diesen context listener
 */
public class ServletRegistratorContextListener implements ServletContextListener {
 private final Logger logger = Logger.getLogger(this.getClass().getName());
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.log(Level.INFO, "registering servlets programmatically using a ServletContextListener now.... " );
        ServletContext context = sce.getServletContext();
        ServletRegistration.Dynamic reg;
        //1.
        reg = context.addServlet("Textservlet1", "org.ragin.helper.TextServlet");
        reg.addMapping("/textdemo1a");
        reg.addMapping("/textdemo1b", "/textdemo1c");
        //2.
        reg = context.addServlet("Textservlet2", org.ragin.helper.TextServlet.class);
        reg.addMapping("/textdemo2a");
        reg.addMapping("/textdemo2b", "/textdemo2c");
        //3
        //3.1
        //so werden Resource Injections nicht laufen:
        //Servlet servlet = new ServletWoche(); 
        //...

        //3.2
        try {
            Servlet servlet = context.createServlet(org.ragin.helper.TextServlet.class);
            reg = context.addServlet("Textservlet32", servlet);
            reg.addMapping("/textdemo31", "/textdemo32");
        } catch (ServletException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

}