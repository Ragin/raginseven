/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.apache.log4j.*;

/**
 * Web application lifecycle listener.
 *
 * @author Connacht
 */
@WebListener
public class RHttpSessionListener implements HttpSessionListener {

    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private int sessionCount = 0;

    public void sessionCreated(HttpSessionEvent event) {
        synchronized (this) {
            sessionCount++;
        }
        logger.log(Level.INFO, "Session Created : " + event.getSession().getId());
        logger.log(Level.INFO, "Total sessions: " + sessionCount);
    }

    public void sessionDestroyed(HttpSessionEvent event) {
        synchronized (this) {
            sessionCount--;
        }
        logger.log(Level.INFO, "Session destroyed :" + event.getSession().getId());
        logger.log(Level.INFO, "Total sessions: " + sessionCount);
    }
}
