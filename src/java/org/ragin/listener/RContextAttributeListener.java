/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.listener;

 
import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.annotation.WebListener;
import org.apache.log4j.*;
//import org.apache.log4j.*;

/**
 * Web application lifecycle listener.
 *
 * @author Connacht
 */
@WebListener
public class RContextAttributeListener implements ServletContextAttributeListener  {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    
 
    @Override
    public void attributeAdded(ServletContextAttributeEvent event) {
       //logger.log(Level.INFO, "attribute added " );
       
    }

    @Override
    public void attributeRemoved(ServletContextAttributeEvent event) {
      //logger.log(Level.INFO, "attribute removed " );
    }

    @Override
    public void attributeReplaced(ServletContextAttributeEvent arg0) {
        //logger.log(Level.INFO, "attribute replaced " );
   }
}
