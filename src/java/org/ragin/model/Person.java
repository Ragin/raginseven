/*
 * standard Person Java Bean
 * supports EL function
 * TLD:
 *   <description>Ragin Custom Tag Library</description>
 *   <tlib-version>0.1</tlib-version>
 *   <short-name>raginCustomTags</short-name>
 *   <uri>/ragin/org/raginCT</uri>
 *   <function>
 *       <description>a JSTL demo function based on Person class</description>
 *       <name>identifyDemoPerson</name>
 *       <function-class>org.ragin.model.Person</function-class>
 *       <function-signature>java.lang.String identifyDemoPerson()</function-signature>
 *   </function>
 *
 * JSP:
 *       <%@ taglib prefix="ragin" uri="/ragin/org/raginCT" %>
 *       <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 *       <!-- call EL function of Person -->
 *       <p>${ragin:identifyDemoPerson()}</p>
 */

package org.ragin.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import org.apache.log4j.*;

@Entity
public class Person implements Serializable {
    @Transient
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    public enum Sex {
        MALE, FEMALE
    }

    
    @Id
    private int id; // the id
    private long epochCreated;
    private long epochLastAccessed;
    private String address;
    private String city;
    private String cob; // city of birth
    private String comment;
    private String country;
    private String dob; // day of birth
    private String email;
    private String imageName;  
    private String lastName;
    private String middleName;
    private String mobile;
    private String phone;
    private String preName;
    private Sex gender;
    private String title;
    private String website;
    private String zip;
    private LocalDate birthday;
   

    /**
     * no-arg constructor
     */
    public Person() {
         
    }
    /**
     * constructor of person
     * @param preName
     * @param lastName
     * @param city 
     * @param gender from enum
     * @param email mail address
     * @param dob day of birth
     * @param comment any comment
     */
    public Person(String preName, String lastName, String city, Sex gender, 
            String email, LocalDate dob, String comment) {
        this.city = city;
        this.comment = comment;
        this.email = email;
        this.lastName = lastName;
        this.preName = preName;
        this.gender = gender;
         logger.log(Level.INFO, "Persion instantiated ");
         
         
    }

 /**
  * init block that sets demo values to a new person. 
  * middlename is not set
  */
    {
        setPreName("");
        setMiddleName("");
        setLastName("");
        setAddress("");
        setZip("");
        setCity("");
        setCountry("");
        setEmail("");
        setWebsite("");
        setPhone("");
        setMobile("");
        setDob("");
        setTitle("");
        setComment("");
        setImageName("");
        //setBirthday( IsoChronology.INSTANCE.date(1990, 7, 15));
         this.id = zufallszahl(1, 9999999);
    }
    
    
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getEpochCreated() {
        return epochCreated;
    }

    public void setEpochCreated(long epochCreated) {
        this.epochCreated = epochCreated;
    }

    public long getEpochLastAccessed() {
        return epochLastAccessed;
    }

    public void setEpochLastAccessed(long epochLastAccessed) {
        this.epochLastAccessed = epochLastAccessed;
    }

    
    
    
    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }
    public Sex getGender() {
        return gender;
    }

    public void setGender(Sex gender) {
        this.gender = gender;
    }
    
    public String getPreName() {
        return preName;
    }

    public void setPreName(String preName) {
        this.preName = preName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

     public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getCob() {
        return cob;
    }

    public void setCob(String cob) {
        this.cob = cob;
    }

  /**
   * returns the age in years of this person
   * @return 
   */
    public int getAge() {
        return birthday
            .until(IsoChronology.INSTANCE.dateNow())
            .getYears();
    }
    
    /**
     * returns a String saying hello
     * @return 
     */
    public String identiyfy() {
        StringBuilder sb = new StringBuilder(); 
        sb.append("My name is ");
        sb.append(this.getPreName());
        sb.append(this.getMiddleName().length()>0?" " + this.getMiddleName() + " ":" ");
        sb.append(this.getLastName());
        sb.append(" and I live in " + this.getCity()+ ". ");
        sb.append("My age is " + getAge() + " years.");
        return sb.toString(); 
    }
    
    /**
     * returns a single demo Person
     * @return 
     */
    public static Person demoSinglePerson() {
        Person p = new Person("Birgit","Meier","Hattingen",Sex.FEMALE,"birgit@demo.net", IsoChronology.INSTANCE.date(1980, 6, 11),"Person 1");
        return p;
    }
    
    /**
     * creates a single demo Person and returns the String value of its identify method
     * @return 
     */
    public static String identifyDemoPerson() {
        Person p = new Person("Birgit","Meier","Hattingen",Sex.FEMALE,"birgit@demo.net", IsoChronology.INSTANCE.date(1980, 6, 11),"Person 1");
        return p.identiyfy();
    }
    
    
    	// Zufallszahl von "min"(einschließlich) bis "max"(einschließlich)
	// Beispiel: zufallszahl(4,10);
	// Mögliche Zufallszahlen 4,5,6,7,8,9,10
	private static int zufallszahl(int min, int max) {
		Random random = new Random();
		return random.nextInt(max - min + 1) + min;
	}
        
        
    /**
     * returns a list of 4 example Persons
     * @return 
     */
    public static List getDemoPersons() {
         List<Person> demo = new ArrayList<>();
         demo.add(new Person("Birgit","Meier","Hattingen",Sex.FEMALE,"birgit@demo.net", IsoChronology.INSTANCE.date(1980, 6, 11),"Person 1"));
         demo.add(new Person("Hans","Dampf","Bonn",Sex.MALE,"hans@demo.net", IsoChronology.INSTANCE.date(1966, 7, 12),"Person 2"));
         demo.add(new Person("Peter","Pan","Rostock",Sex.MALE,"peter@demo.net", IsoChronology.INSTANCE.date(1950, 5, 14),"Person 3"));
         demo.add(new Person("Sandra","Sauer","Wuppertal",Sex.FEMALE,"sandra@demo.net", IsoChronology.INSTANCE.date(1940, 6, 23),"Person 4"));
         return demo;
    }
}
