/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Frage implements Serializable   {
private final Logger logger = Logger.getLogger(this.getClass().getName());
    private int ID;
    private String text;
    public ArrayList<String> alAntworten;
    private int indexRichtigeAntwort;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setText(String text) {
        this.text = text;
    }


    public void setIndexRichtigeAntwort(int indexRichtigeAntwort) {
        this.indexRichtigeAntwort = indexRichtigeAntwort;
    }

    public String getText() {
        return text;
    }

    public ArrayList getAlAntworten() {
        return alAntworten;
    }

    public void setAlAntworten(ArrayList<String> alAntworten) {
        this.alAntworten = alAntworten;
    }


    public int getIndexRichtigeAntwort() {
        return indexRichtigeAntwort;
    }

    public Frage(int ID, String text, int indexRichtigeAntwort) {
        this.ID = ID;
        this.text = text;
        this.indexRichtigeAntwort = indexRichtigeAntwort;
    }

    public Frage() {
        alAntworten = new ArrayList<String>(); 
         //logger.log(Level.INFO, "Frage instantiated ");
    }

    @Override
    public String toString() {
        return "\r\nFrage{" + "ID=" + ID + ", text=" + text + ", \r\nalAntworten=" + alAntworten + ", indexRichtigeAntwort=" + indexRichtigeAntwort + '}';
    }

 

    /**
     * returns a arraylist of example Fragen
     *
     * @return
     */
    public ArrayList<Frage> initFragenkatalogFromCSV(String CSVPath) {
        ArrayList<Frage> fragenliste = new ArrayList<Frage>();
        BufferedReader raginBuffer = null;
        int counter = 0; 
        try {
            String raginCSV;
            raginBuffer = new BufferedReader(new InputStreamReader(new FileInputStream(CSVPath), "UTF-8"));
            while ((raginCSV = raginBuffer.readLine()) != null) {
                counter++;
                //logger.log(Level.INFO, "Init Frage from " + raginCSV);
                Frage curF = new Frage();
                ArrayList<String> tmpAntworten = new ArrayList<String>();
                if (raginCSV != null) {
                    String[] splitData = raginCSV.split("\\s*,\\s*");
                    for (int i = 0; i < splitData.length; i++) {
                        if (!(splitData[i] == null) || !(splitData[i].length() == 0)) {
                            switch (i) {
                                case 0: // id
                                    curF.setID(Integer.parseInt(splitData[i]));
                                    break;
                                case 1: // frage
                                    curF.setText(splitData[i]);
                                    //logger.log(Level.INFO, "added question: " + curF.getText());
                                    break;
                                case 2: // korrekter index
                                    curF.setIndexRichtigeAntwort(Integer.parseInt(splitData[i]));
                                    break;
                                default:
                                    tmpAntworten.add(splitData[i]);
                                    break;
                            }
                        }
                    }
                }
                curF.setAlAntworten(tmpAntworten);
            fragenliste.add(curF);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (raginBuffer != null) {
                    raginBuffer.close();
                }
            } catch (IOException raginException) {
                raginException.printStackTrace();
            }
        }
        logger.log(Level.INFO, "Loaded " + counter + " questions.");
        return fragenliste;
    }

}
