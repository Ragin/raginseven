/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.model;

import java.io.Serializable;
import org.apache.log4j.*;

/**
 *
 * @author mmueller9
 */
public class Antwort implements Serializable  {
    private int ID;
    private int antwort;
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    
    public Antwort() {
         logger.log(Level.DEBUG, "Antwort instantiated ");
    }
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getAntwort() {
        return antwort;
    }

    public void setAntwort(int antwort) {
        this.antwort = antwort;
    }

    @Override
    public String toString() {
        return "Antwort{" + "ID=" + ID + ", antwort=" + antwort + '}';
    }
}
