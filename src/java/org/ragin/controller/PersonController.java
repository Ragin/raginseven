/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.ragin.model.Person;



/**
 *
 * @author Connacht
 */
@WebServlet(name = "PersonController", urlPatterns = {"/PersonController"})
public class PersonController extends HttpServlet {
 private final Logger logger = Logger.getLogger(this.getClass().getName());
    

 @PersistenceUnit(unitName = "RaginSevenPU")
 private EntityManagerFactory emf; 
 
     @PersistenceContext
    private EntityManager em; 
     
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
 

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");
        logger.log(Level.INFO, "processing request.  "  );
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PersonController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PersonController at " + request.getContextPath() + "</h1>");
            
            printFormVars(request, response.getWriter());
            // construct person instance
            Person p = new Person();
            p.setAddress(request.getParameter("Title"));
            //p.setBirthday(request.getParameter("DayOfBirth"));
            p.setPreName(request.getParameter("Prename"));
            p.setMiddleName(request.getParameter("MiddleName"));
            p.setLastName(request.getParameter("LastName"));
            p.setAddress(request.getParameter("Address"));
            p.setCity(request.getParameter("City"));
            p.setCountry(request.getParameter("Country"));
            
            p.setCob(request.getParameter("COB"));
            p.setMobile(request.getParameter("MobilePhone"));
            p.setPhone(request.getParameter("Phone"));
            p.setEmail(request.getParameter("Email"));
            p.setWebsite(request.getParameter("Website"));
            p.setImageName(request.getParameter("ImageName"));
            p.setZip(request.getParameter("ImageName"));
            p.setComment(request.getParameter("Comment"));
            p.setEpochCreated(new Date().getTime());
            
            p.setEpochLastAccessed(new Date().getTime());
            try {
            //EntityManagerFactory emf = Persistence.createEntityManagerFactory("RaginSevenPU");
            
            //logger.log(Level.INFO, "EMF open:   " + emf.isOpen()  );
            //EntityManager em = emf.createEntityManager(); 
            em = emf.createEntityManager();    
            //em.getTransaction().begin();
            em.persist(p);
             logger.log(Level.INFO, "JPA persisted.  "  );
            //em.getTransaction().commit();
            em.close();
             out.println("<h3>Person persisted. </h3>");
            } catch (Exception ex) {
                logger.log(Level.ERROR, "JPA error:  " + ex.getMessage());
                 out.println("<h3>JPA Error: " + ex.getMessage() + "</h3>");
            }
            out.println("<pre>" + p.identiyfy() + "</pre>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    
    
        private void printFormVars(HttpServletRequest request, PrintWriter pw) {
        java.util.Enumeration enu = request.getParameterNames();
        pw.println("<table border=1>");
        while (enu.hasMoreElements()) {
            String paramName = (String) enu.nextElement();
            pw.println("<tr><td>" + paramName + "\n</td>");
            String[] paramValues = request.getParameterValues(paramName);
            if (paramValues.length == 1) {
                String paramValue = paramValues[0];
                if (paramValue.length() == 0) {
                    pw.print("<td><i>No Value</i></td>");
                    logger.log(Level.INFO, paramName + " Param: empty");
                } else {
                    pw.print("<td><i>" + paramValue + "</i></td>");
                    logger.log(Level.INFO, paramName + " Param: " + paramValue);
                }
            } else {
                pw.println("<td><ul>");
                for (int i = 0; i < paramValues.length; i++) {
                    pw.println("<li>" + paramValues[i] + "</li>");
                    logger.log(Level.INFO, "Param: " + paramValues[i]);
                    logger.log(Level.INFO, paramName + " Param: " + paramValues[i]);
                }
                pw.println("</ul></td>");
            }
            pw.println("</tr>");
        }

        pw.println("</table>");
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Handles person actions";
    }// </editor-fold>

}
