/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.*;

/**
 *
 * @author mmueller9
 */
@WebServlet(
        name = "FrontController",
        displayName = "FrontController",
        description = "the main handler of the whole application",
        urlPatterns = {"/FrontController",
            "/appLog",
            "/clearAppLog",
            "/clearServerLog",
            "/examNew",
            "/index",
            "/listPersons",
            "/login",
            "/lform",
            "/logout",
            "/newPerson",
            "/questionsShowDatabase",
            "/reg",
            "/registration",
            "/serverLog",
            "/showPerson",
            "/showSystem",
            "/success",
            "/upload",
            "/welcomePageWithExamples"
        },
        loadOnStartup = 1,
        initParams = {
            @WebInitParam(name = "exampleParam1", value = "D:/FileUpload", description = "not used"),
            @WebInitParam(name = "exampleParam2", value = "jpg,jpeg,gif,png", description = "not used")
        },
        asyncSupported = true
)
//@RunAs("guest") // this role must be defined within the container configuration!
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 15, // 15 MB
        location = "D:/Uploads"
)

public class FrontController extends HttpServlet {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FrontController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FrontController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    @SuppressWarnings("unused")
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int unUsedVariable = 34;

        Cookie cookie = null;
        //Get an array of Cookies associated with this domain
        boolean newCookie = false;

        HttpSession session = request.getSession(true);
        if (session.isNew()) {
            logger.log(Level.INFO, "New session: " + session.getId());

        } else {
            // logger.log(Level.INFO, "Existing session: " + session.getId());

        }

// Check for cookies
        Cookie[] cookies = request.getCookies();
        boolean foundCookie = false;

        for (Cookie cookie1 : cookies) {
            if (cookie1.getName().equals("raginurl")) {
                //out.println("bgcolor = " + newRaginCookie.getValue());
//                logger.log(Level.INFO, "ragin cookie exists. name =  " + cookie1.getName());
//                logger.log(Level.INFO, "ragin cookie exists. value =  " + cookie1.getValue());
//                logger.log(Level.INFO, "ragin cookie exists. comment =  " + cookie1.getComment());
                foundCookie = true;
            } else {
                //logger.log(Level.INFO, "found cookie named =  " + cookie1.getName());
            }
        }

        if (!foundCookie) {
            Cookie newRaginCookie = new Cookie("raginurl", "www.ragin.org");
            newRaginCookie.setMaxAge(1 * 60 * 60); // stunden, minuten, sekunden
            newRaginCookie.setComment("das ist ein Test");
            //newRaginCookie.setDomain("ragin.org"); // the domain name within which this cookie is visible
            newRaginCookie.setSecure(false); // send cookie on all protocols, not only secure
            newRaginCookie.setPath("/");  // The cookie is visible to all the pages in the directory you specify, and all the pages in that directory's subdirectories
            response.addCookie(newRaginCookie);
            logger.log(Level.INFO, "creating ragin cookie  value =  " + newRaginCookie.getValue());
        }

        String requestUri = request.getRequestURI();

        logger.log(Level.INFO, "---> " + requestUri);

        if (requestUri.contains("/examFinished")) {
            RequestDispatcher rd = request.getRequestDispatcher("/EvaluateController");
            rd.forward(request, response);
        } else if (requestUri.contains("/RSecurity")) {
            RequestDispatcher rd = request.getRequestDispatcher("/RSecurity");
            rd.forward(request, response);
        } else if (requestUri.contains("/index")) {
            RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/showSystem")) {
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/showsystem.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/welcomePageWithExamples")) {
            RequestDispatcher rd = request.getRequestDispatcher("/welcome.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/questionsShowDatabase")) {
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/questionsShowDatabase.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/examNew")) {
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/examNew.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/appLog")) {
            logger.log(Level.INFO, "appLog request");
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/appLog.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/clearServerLog")) {
            logger.log(Level.INFO, "clearing system log not implemented yet");
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/serverLog.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/serverLog")) {
            logger.log(Level.INFO, "appLog request");
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/serverLog.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/lform")) { // must be first!!
            logger.log(Level.INFO, "need a login form");
            //System.out.println("need a login form");
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/loginform.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/login")) { // must be second
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
            rd.forward(request, response);

        } else if (requestUri.contains("/logout")) {
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/logout.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/success")) {
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/success.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/registration")) {
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/registration.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/reg")) {
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/reg.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/upload")) {
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/upload.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/clearAppLog")) {
            // clear log file
            FileOutputStream writer = new FileOutputStream(System.getProperty("application-logfile-path"));
            writer.write(("*** Logfile cleared ***\r\n").getBytes());
            writer.close();
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/appLog.jsp");
            rd.forward(request, response);

        } else if (requestUri.contains("/showPerson")) {
//            String indexParam = request.getParameter("index");
//            int index = Integer.parseInt(indexParam);
//            HttpSession session = request.getSession();
//            Memory game = (Memory) session.getAttribute("game");
//            if(game!=null) {
//                game.openImage(index);
//            }
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/showperson.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/listPersons")) {
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/listpersons.jsp");
            rd.forward(request, response);
        } else if (requestUri.contains("/newPerson")) {
//            String paramLevel = request.getParameter("level");
//            HttpSession session = request.getSession();
//            session.setAttribute("game", game);
                logger.log(Level.INFO, "---> forwarding to JSP"  );
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/newperson.jsp");
            rd.forward(request, response);

        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
