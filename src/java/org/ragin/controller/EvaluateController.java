/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.ragin.model.Antwort;

@WebServlet(
        name = "EvaluateController",
        displayName = "EvaluateController",
        description = "evaluates the given exam answers",
        urlPatterns = {"/examFinished"},
        loadOnStartup = 3,
        initParams = {
            @WebInitParam(name = "exampleParam1", value = "ragin", description = "not used"),
            @WebInitParam(name = "exampleParam2", value = "jpg,jpeg,gif,png", description = "not used")
        },
        asyncSupported = true
)
/**
 * servlet evaluates exam answers
 */
public class EvaluateController extends HttpServlet {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EvaluateController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EvaluateController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        PrintWriter pw = response.getWriter();
        response.setContentType("text/html");

        Enumeration en = request.getParameterNames();
        if (request.getParameterMap().containsKey("debugOnly")) {
            // do debug params only
            pw.println("Evaluate Controller");
            printFormVars(request, pw);
            pw.close();
        } else {
            ArrayList<Antwort> alAntwort = new ArrayList<Antwort>();
            java.util.Enumeration enu = request.getParameterNames();
            while (enu.hasMoreElements()) {
                String paramName = (String) enu.nextElement();
                if (paramName.startsWith("q_")) {
                    Antwort currentAntwort = new Antwort();

                    currentAntwort.setID(Integer.parseInt(paramName.substring(2))); //remove prefix

                    // strip prefix
                    String antwortId = paramName.substring(2);
                    currentAntwort.setID(Integer.parseInt(antwortId));

                    currentAntwort.setAntwort(Integer.parseInt(request.getParameter(paramName)));
                    logger.log(Level.DEBUG, "Frage " + currentAntwort.getID() + ", Antwort: "
                            + currentAntwort.getAntwort()
                    );
                    alAntwort.add(currentAntwort);
                }
            }
            logger.log(Level.INFO, "Antworten verarbeitet: " + alAntwort.size());
            session.setAttribute("examAntworten", alAntwort);
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/jsp/examResults.jsp");
            rd.forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "evaluate exam results";
    }// </editor-fold>

    private void printFormVars(HttpServletRequest request, PrintWriter pw) {
        java.util.Enumeration enu = request.getParameterNames();
        pw.println("<table border=1>");
        while (enu.hasMoreElements()) {
            String paramName = (String) enu.nextElement();
            pw.println("<tr><td>" + paramName + "\n</td>");
            String[] paramValues = request.getParameterValues(paramName);
            if (paramValues.length == 1) {
                String paramValue = paramValues[0];
                if (paramValue.length() == 0) {
                    pw.print("<td> No Value </td>");
                    logger.log(Level.INFO, paramName + " Param: empty");
                } else {
                    pw.print("<td> " + paramValue + " </td>");
                    logger.log(Level.INFO, paramName + " Param: " + paramValue);
                }
            } else {
                pw.println("<td><ul>");
                for (int i = 0; i < paramValues.length; i++) {
                    pw.println("<li>" + paramValues[i] + "</li>");
                    logger.log(Level.INFO, "Param: " + paramValues[i]);
                    logger.log(Level.INFO, paramName + " Param: " + paramValues[i]);
                }
                pw.println("</ul></td>");
            }
            pw.println("</tr>");
        }

        pw.println("</table>");
    }
}
