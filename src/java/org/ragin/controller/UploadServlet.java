/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import javax.annotation.security.DeclareRoles;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

@DeclareRoles({"developer", "admin"})

@ServletSecurity(
         httpMethodConstraints = {
             // only authenticated users with role admin can access POST method
             @HttpMethodConstraint (value = "GET", rolesAllowed = "admin"), // nur admin darf GET
             // all can access POST method
             @HttpMethodConstraint (value = "POST"), // jeder darf mit Post zugreifen
             // no one can access trace method
             @HttpMethodConstraint(value="TRACE", emptyRoleSemantic=ServletSecurity.EmptyRoleSemantic.DENY)
         }
)
        
/*
@ServletSecurity(
        value = @HttpConstraint( // Einstellung gilt für ALLE zugriffsmethoden
                
                // <auth-constraint/>  ohne Rollen (zugriff sperren) entspricht:
                // value = ServletSecurity.EmptyRoleSemantic.DENY // Zugriff komplett sperren
                //
                // <auth-constraint>
                // <role-name>developer</role-name>
                // <role-name>admin</role-name>
                // </auth-constraint> entspricht
                rolesAllowed = {"admin", "developer"},  // authorisierung
                // aktivierung der transport-garantie:
                transportGuarantee = ServletSecurity.TransportGuarantee.CONFIDENTIAL // SSL
                //transportGuarantee = ServletSecurity.TransportGuarantee.NONE // keine verschlüsselung
        )
)
*/
@MultipartConfig(
        fileSizeThreshold = 100_000,
        //location = "C:\\tmp",
        maxFileSize = 100_000_000_000_000L,
        maxRequestSize = 100_000_000_000_000L
)


/*
    <servlet>
        <servlet-name>UploadServlet</servlet-name>
        <servlet-class>org.ragin.controller.UploadServlet</servlet-class>
        <init-param>
            <param-name>uploadverzeichnis</param-name>
            <param-value>C:\\tmp</param-value>
        </init-param>
        <load-on-startup>3</load-on-startup>
    </servlet>
 
    <servlet-mapping>
        <servlet-name>UploadServlet</servlet-name>
        <url-pattern>/UploadServlet</url-pattern>
    </servlet-mapping>
*/

@WebServlet(
    name = "UploadServlet", 
    urlPatterns = {"/UploadServlet"},
    initParams = @WebInitParam(name = "uploadverzeichnis", value = "C:\\tmp")
)

public class UploadServlet extends HttpServlet {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * handles file upload
     */
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
//        logger.log(Level.INFO, "entered UploadServlet");
        // gets absolute path of the web application
        String appPath = request.getServletContext().getRealPath("");
        // constructs path of the directory to save uploaded file
        //String savePath = appPath + File.separator + SAVE_DIR;
        String savePath = getInitParameter("uploadverzeichnis");
        // creates the save directory if it does not exists
//        logger.log(Level.INFO, "savePath: " + savePath);
        File fileSaveDir = new File(savePath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdir();
            logger.log(Level.INFO, "Dir created: " + savePath);
        }

        for (Part part : request.getParts()) {
            // String fileName = extractFileName(part);
            String result = process(part);
            logger.log(Level.INFO, "processed:  " + result);
        }

        request.setAttribute("message", "Uploads were stored into directory " + savePath);
        logger.log(Level.INFO, "done ");
        getServletContext().getRequestDispatcher("/WEB-INF/jsp/uploadResult.jsp").forward(
                request, response);
    }

    private String process(Part part) throws IOException {
        if (part.getSize() == 0) {
            return "Parameter " + part.getName() + " nicht gesendet";
        }
        String targetPathName = getInitParameter("uploadverzeichnis");
        Path target = Paths.get(targetPathName, part.getSubmittedFileName());
        try (InputStream is = part.getInputStream()) {
            Files.copy(is, target, StandardCopyOption.REPLACE_EXISTING);
//            logger.log(Level.INFO,  "File written. " );
        }
        return "Hochgeladen: " + part.getSubmittedFileName();
    }

    /**
     * Extracts file name from HTTP header content-disposition wird nicht mehr
     * in neuer servlet api benötigt
     */
    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }
}
