
package org.ragin.encryption;

/*
 Generate a Message Digest
*/
import java.security.*;

public class MessageDigestExample {
    
    public static void main (String[] args) throws Exception {
        //
        // Check args and get plaintext
        if (args.length !=1) {
            System.err.println("Usage: java MessageDigestExample text");
            System.exit(1);
        }
        byte[] plainText = args[0].getBytes("UTF8");
        //
        // Get a message digest object using the MD2 algorithm  BOUNCY
        // MessageDigest messageDigest = MessageDigest.getInstance("MD2");
        //
        // Get a message digest object using the MD5 algorithm
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        //
        // Get a message digest object using the SHA-1 algorithm
        // MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");                        
        //
        // Get a message digest object using the SHA-256 algorithm
        // MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        //
        // Get a message digest object using the SHA-384 algorithm
        // MessageDigest messageDigest = MessageDigest.getInstance("SHA-384");
        //
        // Get a message digest object using the SHA-512 algorithm
        // MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");              
        //
        // Print out the provider used
        System.out.println( "\n" + messageDigest.getProvider().getInfo() );
        //
        // Calculate the digest and print it out
        messageDigest.update( plainText);
        System.out.println( "\nDigest: " );
        System.out.println( new String( messageDigest.digest(), "UTF8") );
    }
}