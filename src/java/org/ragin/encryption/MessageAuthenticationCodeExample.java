
package org.ragin.encryption;
/*
 Generate a Message Authentication Code
*/
 
import javax.crypto.*;

public class MessageAuthenticationCodeExample {
    
    public static void main (String[] args) throws Exception {
        //
        // Check args and get plaintext
        if (args.length !=1) {
            System.err.println("Usage: java MessageAuthenticationCodeExample text");
            System.exit(1);
        }
        byte[] plainText = args[0].getBytes("UTF8");

        System.out.println( "\nStart generating key" );
        //
        // Get a key for the HmacMD5 algorithm      
        KeyGenerator keyGen = KeyGenerator.getInstance("HmacMD5");
        //
        // Get a key for the HmacSHA1 algorithm     
        // KeyGenerator keyGen = KeyGenerator.getInstance("HmacSHA1");      
        SecretKey Key = keyGen.generateKey();
        System.out.println( "Finish generating key" );
        //
        // Get a HmacMD5 object and update it with the plaintext
        Mac mac = Mac.getInstance("HmacMD5");
        //
        // Get a HmacSHA1 object and update it with the plaintext
        // Mac mac = Mac.getInstance("HmacSHA1");       
        mac.init(Key);
        mac.update(plainText);
        //
        // Print out the provider used and the MAC
        System.out.println( "\n" + mac.getProvider().getInfo() );
        System.out.println( "\nMAC: " );
        System.out.println( new String( mac.doFinal(), "UTF8") );
    }
}