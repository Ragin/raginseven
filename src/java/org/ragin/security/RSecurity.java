/*
 * need to have glassfish-web.xml entry: 
 * <security-role-mapping>
 *   <role-name>developer</role-name>
 *   <group-name>developer</group-name>
 * </security-role-mapping>
 *   <security-role-mapping>
 *   <role-name>worker</role-name>
 *   <group-name>worker</group-name>
 * </security-role-mapping>
 *   <security-role-mapping>
 *   <role-name>chief</role-name>
 *   <group-name>chief</group-name>
 * </security-role-mapping>
 */
package org.ragin.security;

import java.io.IOException;
import java.security.CodeSource;
import java.security.Principal;
import javax.security.auth.Subject;
import javax.security.jacc.PolicyContext;
import javax.security.jacc.PolicyContextException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author mmueller9
 */
@WebServlet(name = "RSecurity", urlPatterns = {"/RSecurity"})
public class RSecurity extends HttpServlet {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String target = null;
        //Get all users roles
        Subject subject;
        try {
            subject = (Subject) PolicyContext.getContext("javax.security.auth.Subject.container");
            
            
            Class klass = PolicyContext.class;
            CodeSource codeSource = klass.getProtectionDomain().getCodeSource();
            if ( codeSource != null) {
                //System.out.println(codeSource.getLocation());
                logger.log(Level.INFO, "---> found class in " + codeSource.getLocation());
            }
            
            for (Principal principal : subject.getPrincipals()) {
                logger.log(Level.INFO, "---> found principal:" + principal.getName());
            }
        } catch (PolicyContextException ex) {
            java.util.logging.Logger.getLogger(RSecurity.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        if ((request.isUserInRole("chief"))) {
            logger.log(Level.INFO, "---> role applied: chief");
            target = "/WEB-INF/jsp/securityChief.jsp";
        } else {
            if ((request.isUserInRole("developer"))) {
                logger.log(Level.INFO, "---> role applied: developer");
                target = "/WEB-INF/jsp/securityChief.jsp";
            } else if (request.isUserInRole("worker")) {
                target = "/WEB-INF/jsp/securityWorker.jsp";
            } else {
                throw new IllegalArgumentException("Unbekannte Rolle!");
            }
        }

        logger.log(Level.INFO, "-> has role chief: " + request.isUserInRole("chief"));
        logger.log(Level.INFO, "-> has role worker: " + request.isUserInRole("worker"));
        logger.log(Level.INFO, "-> has role developer: " + request.isUserInRole("developer"));

        request.getRequestDispatcher(target).forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
