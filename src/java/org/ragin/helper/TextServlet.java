package org.ragin.helper;

 import java.io.IOException;
 import java.io.PrintWriter;
 import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
 import javax.servlet.http.HttpServlet;
 import javax.servlet.http.HttpServletRequest;
 import javax.servlet.http.HttpServletResponse;
 
/**
 * part of http servlet response wrapper example
 * generates only a string of mixed case characters
 * @author mmueller9
 */
@WebServlet(
        name = "TextServlet",
        displayName = "TextServlet",
        urlPatterns = {"/text"},
        loadOnStartup = 4,
        initParams = {
            @WebInitParam(name = "exampleParam1", value = "ragin"),
            @WebInitParam(name = "exampleParam2", value = "jpg,jpeg,gif,png")
        },
        asyncSupported = true
)
 public class TextServlet extends HttpServlet {
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/plain;charset=UTF-8");
        
        PrintWriter out = response.getWriter();
        out.print("Hallo Welt. Dieser Text wurde vom Servlet nicht nur gross geschrieben.");
        
        
    }
 }