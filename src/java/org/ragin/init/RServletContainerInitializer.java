/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.init;

/**
 *
 * @author Connacht
 */
 
import java.util.*;
import javax.servlet.*;
import org.apache.log4j.Logger;
/**
 * example how to register servlets using a ServletContainerInitializer
 * @author Connacht
 */
public class RServletContainerInitializer implements ServletContainerInitializer {
     private final Logger logger = Logger.getLogger(this.getClass().getName());
    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        try {
            Class klass = Class.forName("org.ragin.controller.DemoServlet");
            //Class<MyJar1Servlet> clazz = (Class<MyJar1Servlet>)klass;

            Servlet s = ctx.createServlet(klass);
            ServletRegistration.Dynamic d = ctx.addServlet("RaginDemoServlet", s);
            d.addMapping("/demoservlet");
        } catch (ClassNotFoundException e) {
            System.out.println("Error in initializer: " + e.getMessage());
        }
    }
}