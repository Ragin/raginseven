/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ragin.async;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author mmueller9
 */
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;

public class BigJob {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public void doJob(String title, int idleTimeMillis, HttpServletResponse response) {
        long startTime = System.currentTimeMillis();
        
        String message = "Starttime: " + startTime; 
        logger.log(Level.INFO, "BigJob starting: " + startTime);

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            Thread.sleep(idleTimeMillis);
            long endTime = System.currentTimeMillis();
            long duration = endTime - startTime; 
            message = message + ", Endtime: " + endTime + ", Duration: " + duration; 
            out.println("<!DOCTYPE html>");
            out.println("<html><head><title>LongRunningServletDemo</title></head>");
            out.println("<body><h3>" + title  + "</h3>" + "<p>" + message + "</p>"+"</body></html>");
            
            logger.log(Level.INFO, "BigJob ending: " + startTime);
            logger.log(Level.INFO, "BigJob duration: " + duration);
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }

    }

}
