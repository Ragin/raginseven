/*
 * implements an asynchronous servlet using BigJob class
 */
package org.ragin.async;

import java.io.IOException;
import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

@WebServlet(
        name = "LongRunningServlet",
        urlPatterns = {"/LongRunningServlet"},
        asyncSupported = true // this is important!
)
public class LongRunningServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        String time = request.getParameter("time");
        
        int idleTimeMillis = Integer.valueOf(time);
        // max 10 seconds
        if (idleTimeMillis > 10000) {
            idleTimeMillis = 10000;
        }
        final int millis = idleTimeMillis;  // referenced by inner class must be final
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true); // this is for tomcat
        AsyncContext asyncContext = request.startAsync(); // this is for spec

        //Registering a listener with the AsyncContext object to listen to events
        //from the AsyncContext object.
        asyncContext.setTimeout(9000);

        asyncContext.addListener(new AsyncListener() {
            private final Logger logger = Logger.getLogger(this.getClass().getName());
            @Override
            public void onComplete(AsyncEvent event) throws IOException {
                logger.log(Level.INFO, "Async complete");
                //System.out.println("Async complete");
            }

            @Override
            public void onTimeout(AsyncEvent event) throws IOException {
                logger.log(Level.INFO, "Timed out...");
                //System.out.println("Timed out...");
            }

            @Override
            public void onError(AsyncEvent event) throws IOException {
                logger.log(Level.INFO, "Error...");
                //System.out.println("Error...");
            }

            @Override
            public void onStartAsync(AsyncEvent event) throws IOException {
                // recognizes only the start of the second cyclus
                logger.log(Level.INFO, "Starting async... "
                        + "This event will not be recognized as startAsync() "
                        + "is called before registering this listeners.");
                //System.out.println("Starting async...");
            }
        });

        long startTime = System.currentTimeMillis();
        logger.log(Level.INFO, "LongRunningServlet starting: " + startTime);
        // create a new developer thread
        Thread th = new Thread() {
            @Override
            public void run() {
                HttpServletResponse response = (HttpServletResponse) asyncContext.getResponse();
                org.ragin.async.BigJob bj = new org.ragin.async.BigJob();
                bj.doJob("Servlet-ASYNC", millis,  response);
                asyncContext.complete(); // tell thread to send response when finished
                
                // alternativ: 
                // ScheduledThreadPoolExecutor executer = new ScheduledThreadPoolExecutor(10);
                // executer.execute(new DemoAsyncService(ac));
            }
        };

        th.start(); // start developer thread
        // free the containert-thread for new requests
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        logger.log(Level.INFO, "LongRunningServlet ending: " + endTime 
                + ", duration: " + duration);
    }

}
