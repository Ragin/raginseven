/*
 * example of a not async servlet displaying a progress bar.
 * max. number of parallel threads in tomcat is 200...
 * so we should use a async servlet to optimize this
 */
package org.ragin.async;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.stream.Collectors;
import javax.servlet.AsyncContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author mmueller9
 */
@WebServlet(name = "ProgressAsyncServlet", 
        urlPatterns = {"/ProgressAsyncServlet"}, 
        asyncSupported = true // in web.xml: <async-supported>true</async-supported>
)
public class ProgressAsyncServlet extends HttpServlet {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public static String read(InputStream input) throws IOException {
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
            return buffer.lines().collect(Collectors.joining("\n"));
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pw = response.getWriter();

        ServletContext ctx = request.getServletContext();
        InputStream stream = ctx.getResourceAsStream("/WEB-INF/html/head.html");
        logger.log(Level.INFO, "stream: " + stream);
        String headHtml = read(stream);

        pw.println(headHtml);

        //pw.println("<html><head><title>Progress Servlet</title></head><body>"); 
        pw.println("<p>entering doGet(), der request verarbeitende Thread ist: <b> " + Thread.currentThread().getName() + "</b></p>");
        pw.println("<progress id='progress' max='100'></progress>");
        AsyncContext asyncContext = request.startAsync();
        // java 8 syntax
        asyncContext.start(() -> {
            int i = 0;
            while (i <= 100) {
                pw.println("<script>document.getElementById('progress').value=\""
                        + i++ + "\";</script>");
                pw.flush();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }
            pw.println("<script> document.getElementById('progress').style.display='none';</script>");
            pw.println("<h3>Juchu ProgressAsyncServlet!!</h3>");
            pw.println("<p>[we had a long time consuming process and generated + returned a result here]</p>");
            pw.println("<p>exiting doGet(), Der verarbeitende Async Thread ist: <b> " + Thread.currentThread().getName() + "</b></p>");
            pw.println("</body></html>");
            asyncContext.complete();
        }); // end asyncContext

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
