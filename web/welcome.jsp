<%@page import="javax.annotation.Resource"%>
<jsp:include page='WEB-INF/jsp/inc/header.jsp'>
  <jsp:param name='pageName' value='Welcome' />
</jsp:include>
<!-- ------------------------------ welcome.jsp ----------------------------- -->
<%@ taglib prefix="ragin" uri="/org/ragin/seven" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="rt" tagdir="/WEB-INF/tags" %>

<rt:checklogin/>  
<!-- ------------------------------ welcome.jsp ----------------------------- -->
<div align="center"><img class ="autowidthimg" src="${contextPath}/img/tiger-1000.png"> </div>
<hr/>
<h4>Other Example output</h4>


<rt:tests/>
<h4><rt:dummy/></h4>
<%--
--%>

<%!
    // works for tomcat but not for glassfish (--> null)
    // null in Glassfish @Resource(name = "ansprechpartner") // resource injection from container!
    // failed: @Resource(lookup="string/ansprechpartner")
    @Resource(name = "ragin/jndi/ansprechpartner" )
    
    private String asp;
    
%>
<p>Ihr Ansprechpartner: <%= asp %></p>
<hr>
<c:set var="bodyURL" value="WEB-INF/html/url.html" scope="request"></c:set>
<p>Demo: EL innerhalb von jsp:include <br/>
<jsp:include page='${bodyURL}' />
</p>
<p>Demo: Scriptlet innerhalb von jsp:include <br/>
<jsp:include page='<%= (String) request.getAttribute("bodyURL") %>' />
</p>
<rt:foot/>