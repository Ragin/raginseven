<%@ taglib prefix="rt" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="ragin" uri="/org/ragin/seven" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:include page='inc/header.jsp'>
  <jsp:param name='pageName' value='List persons' />
</jsp:include>
<h3><span class="label label-primary">List persons</span></h3>
<rt:foot/>