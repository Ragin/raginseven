<%@ taglib prefix="rt" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="ragin" uri="/org/ragin/seven" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:include page='inc/header.jsp'>
  <jsp:param name='pageName' value='Question database items' />
</jsp:include>
<h2><span class="label label-info">Question database items</span></h2>




<script type="text/javascript">
    $(document).ready(function () {

        $("#item").dblclick(function (event) {
            $("#item").toggle();
        });

    <c:forEach items="${questions}" var="eineFrage">
        $("#questionID${eineFrage.ID}").dblclick(function (event) {
            $("#answerID${eineFrage.ID}").toggle();
        });

    </c:forEach>


    });
</script>



<p id="item">Double click headers to show correct answers..</p>
<form id="formQuestions">
    <c:forEach items="${questions}" var="eineFrage">
    <!------------------------------- Question ${eineFrage.ID} -------------------------------->
        <h3 id="questionID${eineFrage.ID}" >
            <span class="glyphicon glyphicon-edit" aria-hidden=true></span>
            <span class="label label-default">${eineFrage.text}</span></h3>
        <div class="input-group" id="question${eineFrage.ID}" >
            <span class="input-group-addon">
                <c:forEach items="${eineFrage.alAntworten}" begin="0" var="antwort" varStatus="count">
                    <div class="checkbox" align="left" >
                        <label><input id="question${eineFrage.ID}"  type="checkbox" value="${count.index +1}">${count.index +1}: ${antwort}</label></div>
                        </c:forEach> 
            </span>
        </div><!-- /input-group -->            
        <h5 id="answerID${eineFrage.ID}" style="display:none;">Korrekte Antwort: ${eineFrage.indexRichtigeAntwort +1} -> ${eineFrage.alAntworten[eineFrage.indexRichtigeAntwort  ]}</h5>

    </c:forEach>
    <div align="center">
        <br/>
        <button type="submit" class=".btn-success">Submit my answers...</button>
    </div>
</form>



<rt:foot/>

