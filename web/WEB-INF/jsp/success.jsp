<%@ taglib prefix="rt" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="ragin" uri="/org/ragin/seven" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page import="java.io.File"%>
<jsp:include page='inc/header.jsp'>
  <jsp:param name='pageName' value='Successfully logged in' />
</jsp:include>
<h2><span class="label label-info">Successfully logged in</span></h2>


<%
    if ((session.getAttribute("userid") == null) || (session.getAttribute("userid") == "")) {
%>
<p>You are not logged in<br/>
<a href="lform">Please Login</a></p>
<%} else {
%>
<h3>Welcome <%=session.getAttribute("userid")%></h3>
<%
    }
%>


<rt:foot/>

