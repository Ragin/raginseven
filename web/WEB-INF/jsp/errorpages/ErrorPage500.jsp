<%@page import="java.io.PrintWriter"%>
<%@ page isErrorPage="true" %>
<%@include file="../inc/header.jsp" %>

<!-- ErrorPage500.jsp -->

<% response.setStatus(500); %>


<c:out value="${pageContext.exception.message}" />

<table class="table table-striped ragintable">
    <tr valign="top">
        <td width="40%"><h3><span class="label label-danger">5xx Server error</span></h3></td>
        <td>

            <p>Message: ${pageContext.exception.message}</p>

        </td>
    </tr>
    <tr valign="top">
        <td><b>URI:</b></td>
        <td>${pageContext.errorData.requestURI}</td>
    </tr>
    <tr valign="top">
        <td><b>Status code:</b></td>
        <td>${pageContext.errorData.statusCode}</td>
    </tr>
    <tr valign="top">
        <td><b>Stack trace:</b></td>
        <td>
            <pre>
                <% exception.printStackTrace(new PrintWriter(out));%>
            </pre>
        </td>
    </tr>
</table>
<rt:foot/>