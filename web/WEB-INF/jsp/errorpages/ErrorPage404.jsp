<%@page import="java.io.PrintWriter"%>
<%@ page isErrorPage="true" %>
<% response.setStatus(404);%>
<%@include file="../inc/header.jsp" %>
<!-- ErrorPage404.jsp -->
<table class="table table-striped ragintable">
    <tr valign="top">
        <td width="40%"><h3><span class="label label-danger">404 - not found.</span></h3></td>
        <td>
            <p>Message: ${pageContext.exception.message}</p>

            ${pageContext.exception}</td>
    </tr>
    <tr valign="top">
        <td><b>URI:</b></td>
        <td>${pageContext.errorData.requestURI}</td>
    </tr>
    <tr valign="top">
        <td><b>Status code:</b></td>
        <td>${pageContext.errorData.statusCode}</td>
    </tr>
    <tr valign="top">
        <td><b>Stack trace:</b></td>
        <td>
            <pre>
                       not implemented yet 
            </pre>
        </td>
    </tr>
</table>
<rt:foot/>