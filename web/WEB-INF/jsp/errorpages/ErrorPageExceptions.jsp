<%@page import="java.io.PrintWriter"%>
<%@ page isErrorPage="true" %>
<%@include file="../inc/header.jsp" %>
<% response.setStatus(500); %>

<!-- ErrorPageException.jsp -->


        <h3>Exception!!!</h3>
        <p>Message: ${pageContext.exception.message}</p>
       

        <h1>oje....</h1>
        <table width="100%" border="1">
            <tr valign="top">
                <td width="40%"><b>Error:</b></td>
                <td>${pageContext.exception}</td>
            </tr>
            <tr valign="top">
                <td><b>URI:</b></td>
                <td>${pageContext.errorData.requestURI}</td>
            </tr>
            <tr valign="top">
                <td><b>Status code:</b></td>
                <td>${pageContext.errorData.statusCode}</td>
            </tr>
            <tr valign="top">
                <td><b>Stack trace:</b></td>
                <td>
                    <pre>
                        <% exception.printStackTrace(new PrintWriter(out));%>
                    </pre>
                </td>
            </tr>
        </table>
<rt:foot/>