<%@ taglib prefix="rt" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="ragin" uri="/org/ragin/seven" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page import="java.io.File"%>
<jsp:include page='inc/header.jsp'>
  <jsp:param name='pageName' value='New exam' />
</jsp:include>

<rt:checklogin/>  


<h3><span class="label label-primary">Answer the following questions...</span></h3>
    
<form action="examFinished" method="post" autocomplete="off">
   <button name="subButton1" id="subButton1" type="submit"   class=".btn-success">Submit my answers...</button>  
    <c:forEach items="${questions}" var="eineFrage">
    <!------------------------------- Question ${eineFrage.ID} -------------------------------->
        <h3 id="questionID${eineFrage.ID}" >
            <img src="${contextPath}/img/smiley-o-xs.jpg">
            <!--
            <span class="glyphicon glyphicon-edit" aria-hidden=true></span>
            -->
            <span class="label label-default">${eineFrage.text}</span></h3>
        <div class="input-group" id="q_${eineFrage.ID}" name="q_${eineFrage.ID}">
            <span class="input-group-addon">
                <c:forEach items="${eineFrage.alAntworten}" begin="0" var="antwort" varStatus="count">
                    <div class="checkbox" align="left" >
                        <label><input name="q_${eineFrage.ID}"  id="q_${eineFrage.ID}_${count.index +1}"  
                                      onclick="$( '#q_${eineFrage.ID}_x' ).attr('checked', false);"
                                      
                                      type="radio" 
                                      value="${count.index }">${count.index  }: ${antwort}</label></div>
                </c:forEach> 
                <div class="checkbox" align="left" >
                    <label><input name="q_${eineFrage.ID}"  id="q_${eineFrage.ID}_x"  
                                  type="radio" 
                                  value="99" checked="checked">no idea</label>
                </div>
        
            </span>
        </div><!-- /input-group -->            
        <h5 name="answerID${eineFrage.ID}" id="answerID${eineFrage.ID}" style="display:none;">Korrekte Antwort: ${eineFrage.indexRichtigeAntwort +1} -> ${eineFrage.alAntworten[eineFrage.indexRichtigeAntwort  ]}</h5>

    </c:forEach>
<label><input name="debugOnly"  id="debugOnly"  type="checkbox" value="debugOnly" >debug params only</label>        
    <div align="center">
        <br/>
        <button name="subButton2" id="subButton2" type="submit" class=".btn-success">Submit my answers...</button>
    </div>
</form>



<rt:foot/>