<%@ taglib prefix="rt" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="ragin" uri="/org/ragin/seven" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:include page='inc/header.jsp'>
  <jsp:param name='pageName' value='Logged out' />
</jsp:include>
<!-- ------------------------------ welcome.jsp ----------------------------- -->

<%@ page import ="java.sql.*" %>
<h3><span class="label label-primary">Successfully logged out</span></h3>
<%
//session.setAttribute("userid", null);
 session.invalidate();
%> 
<rt:foot/>  