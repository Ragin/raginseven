<%@page import="java.util.LinkedList"%>
<%@page import="java.util.List"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>
<%@ taglib prefix="rt" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="ragin" uri="/org/ragin/seven" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page import="java.io.File"%>
<jsp:include page='inc/header.jsp'>
  <jsp:param name='pageName' value='Server Log' />
</jsp:include>
<c:set var="contextPath" value="${pageContext.request.contextPath}" scope="application"/>

<header>
    <hgroup>
        <h3><span class="label label-default">Log4J Logfile at <%= new java.util.Date()%></span></h3> 
    </hgroup>
</header>
<div align="center">
    <textarea>
        <%
            //String txtFilePath = "C:\\Program Files\\glassfish-4.1.1\\glassfish\\domains\\domain1\\config\\logs\\raginwebapp2.log";
            ServletContext ctx = config.getServletContext(); 
            String txtFilePath = pageContext.getServletContext().getInitParameter("pathServerLog") ; 
            //String txtFilePath = "C:\\Program Files\\glassfish-4.1.1\\glassfish\\domains\\domain1\\logs\\server.log";
            
            BufferedReader reader = new BufferedReader(new FileReader(txtFilePath));
            //BufferedReader br = new InputStreamReader(new FileInputStream(txtFilePath));
            //StringBuilder sb = new StringBuilder(); // reading whole file
            StringBuilder sb2 = new StringBuilder();// kind of tail
            String line;

            List<String> lines = new LinkedList<String>();
            for(String tmp; (tmp = reader.readLine()) != null;) 
                if (lines.add(tmp) && lines.size() > 200) 
                    lines.remove(0);            
            
            /*
            reading whole file
            while ((line = reader.readLine()) != null) {
                // prepend so the last is at the top
                sb.insert(0,line + "\n");
            }
            //out.println(sb.toString() + "</textarea>");
            */
            
            for(String singleline : lines)
                //sb2.insert(0,singleline + "\n"); // reverse order
                sb2.append(singleline + "\n"); // lasted below
            out.println(sb2.toString() + "</textarea>");
            out.println( "<p> File: " + txtFilePath + "</p>");            
          
        %>
    <img src="${contextPath}/img/smiley-o.jpg">
</div>
<rt:foot/>