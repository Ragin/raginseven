<%@ taglib prefix="rt" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="ragin" uri="/org/ragin/seven" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:include page='inc/header.jsp'>
  <jsp:param name='pageName' value='Exam results' />
</jsp:include>
<h3><span class="label label-primary">Exam Results</span></h3>

<c:set var="passingScore" value="0" scope="page" />
<c:set var="countAll" value="0" scope="page" />
<c:set var="countOK" value="0" scope="page" />
<c:set var="contextPath" value="${pageContext.request.contextPath}" scope="application"/>

<c:forEach items="${sessionScope.examAntworten}" var="antwort">
    <c:set var="countAll" value="${countAll + 1}" scope="page"/>
    <c:set var="fragestellung" value="${applicationScope.questions}"/>


    <h3>  ${antwort.ID}: ${applicationScope.questions[antwort.ID-1].text} </h3>

    <c:set var="intLsg" value="${antwort.antwort}" />
    <c:set var="intAnt" value="${applicationScope.questions[antwort.ID -1].indexRichtigeAntwort}" />
    <c:if test = "${intLsg == intAnt}">
        <c:set var="countOK" value="${countOK + 1}" scope="page"/>
        <p class="einrueckung"><span class="glyphicon glyphicon-ok" style="color:green"></span>
        </c:if>
        <c:if test = "${intLsg != intAnt}">
        <p class="einrueckung"><span class="glyphicon glyphicon-remove-sign" style="color:red"></span>
        </c:if>
        Ihre Antwort: ${antwort.antwort} :  ${applicationScope.questions[antwort.ID -1].alAntworten[antwort.antwort]} 
    </p>
    <c:if test = "${intLsg != intAnt}">
        <p class="einrueckung"><span class="glyphicon glyphicon-info-sign" style="color:orange"></span>            

            L&ouml;sung:
            ${applicationScope.questions[antwort.ID -1].indexRichtigeAntwort} 
            :
            ${applicationScope.questions[antwort.ID -1].alAntworten[applicationScope.questions[antwort.ID -1].indexRichtigeAntwort]} 
        </p>      
    </c:if>        


</c:forEach> 

<fmt:formatNumber var="ergebnisGerundet"
                  value="${countOK div countAll * 100}"
                  maxFractionDigits="0" />        

<div align="center">
    <c:if test = "${ergebnisGerundet > initParam.passingScore}">
        <div align="center">
            <img src="${contextPath}/img/smiley-passed.jpg">
        </div>
        <h3><span class="label label-success">Congratulation!</span></h3>
    </c:if>

    <c:if test = "${ergebnisGerundet < initParam.passingScore}">

        <img src="${contextPath}/img/smiley-failed.jpg">

        <h3><span class="label label-danger">Unfortunately failed.</span></h3>
    </c:if>        



    <p>You scored ${ergebnisGerundet} percent </p>
    <p>Passing Score: ${initParam.passingScore} %</p>

</div>
<!-- debugging objects into textarea
<textarea rows="8">${applicationScope.questions}</textarea>
<hr/>
<textarea>${sessionScope.examAntworten}</textarea>
-->
<rt:foot/>