<%@ taglib prefix="rt" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="ragin" uri="/org/ragin/seven" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:include page='inc/header.jsp'>
  <jsp:param name='pageName' value='New Person' />
</jsp:include>
<h3>New person</h3>
<rt:personCreate/>        
<rt:foot/>