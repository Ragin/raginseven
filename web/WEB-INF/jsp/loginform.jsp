<%@ taglib prefix="rt" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="ragin" uri="/org/ragin/seven" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:include page='inc/header.jsp'>
  <jsp:param name='pageName' value='Login Form' />
</jsp:include>
 
<!-- ------------------------------ loginform.jsp ----------------------------- -->

<h3><span class="label label-primary">Um diese Funktion zu nutzen m&uuml;ssen Sie sich anmelden.</span></h3>

  <form method="GET" action="login">
            <center>
            <table border="1" width="30%" cellpadding="3">
             
                <tbody>
                    <tr>
                        <td>Benutzername</td>
                        <td><input type="text" name="uname" value="" /></td>
                    </tr>
                    <tr>
                        <td>Passwort</td>
                        <td><input type="password" name="pass" value="" /></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Login" /></td>
                        <td><input type="reset" value="Reset" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">Noch nicht registriert? <a href="reg">Jetzt registrieren</a></td>
                    </tr>
                </tbody>
            </table>
            </center>
        </form>

<rt:foot/>