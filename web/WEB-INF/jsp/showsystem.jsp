<%@page import="java.util.TreeMap"%>
<%@ taglib prefix="rt" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="ragin" uri="/org/ragin/seven" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@page import="java.io.File"%>
<jsp:include page='inc/header.jsp'>
  <jsp:param name='pageName' value='Log4J Config' />
</jsp:include>

<%-- for length function --%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<a name="log4"></a>
<h3><span class="label label-primary">Log4J properties</span></h3>

<textarea>
    ${applicationScope.log4jConfiguration}
 
</textarea>

<a name="container"></a>
<h3><span class="label label-primary">Container values</span></h3>
<table class="table table-striped ragintable" >


    <tr>
        <td class="schluesselwert">ServerInfo</td>
        <td class="erzwungenerUmbruch">${applicationScope.serverInfo}</td>
    </tr>
    <tr>
        <td class="schluesselwert">Servlet API Major Version</td>
        <td class="erzwungenerUmbruch">${applicationScope.servletAPIMajorVersion}</td>
    </tr>
    <tr>
        <td class="schluesselwert">Servlet API Minor Version</td>
        <td class="erzwungenerUmbruch">${applicationScope.servletAPIMinorVersion}</td>
    </tr>

    <tr>
        <td class="schluesselwert">App Logfile Size</td>
        <td class="erzwungenerUmbruch"><%
            String pfad = application.getInitParameter("pathAppLog");
            out.print(new File(pfad).length() + " byte in file " + pfad);
            %></td>
    </tr>
        <tr>
        <td class="schluesselwert">Server Logfile Size</td>
        <td class="erzwungenerUmbruch"><%
            pfad = application.getInitParameter("pathServerLog");
            out.print(new File(pfad).length() + " byte in file " + pfad);
            %></td>
    </tr>
</table>

<a name="params"></a>
<h3><span class="label label-primary">InitParam values</span></h3>
<table class="table table-striped ragintable" >

    <c:forEach items="${initParam}" var="p">
        <tr>
            <td class="schluesselwert">${p.key}</td>
            <td class="erzwungenerUmbruch">${p.value}</td>
        </tr>
    </c:forEach>
</table>

<a name="attributes"></a>
<h3><span class="label label-primary">Application attributes</span></h3>
<table class="table table-striped ragintable" >
    <tr><td class="schluesselwert">Exam size:</td><td class="erzwungenerUmbruch">${fn:length(applicationScope.questions)} questions</td></tr>
    <tr><td class="schluesselwert">\${applicationScope.contextPath}</td><td class="erzwungenerUmbruch">${applicationScope.contextPath}</td></tr>
    <tr><td class="schluesselwert">\${applicationScope.persons}</td><td class="erzwungenerUmbruch">${applicationScope.persons}</td></tr>
    <tr><td class="schluesselwert">\${applicationScope['org.glassfish.jsp.isStandaloneWebapp']}</td><td class="erzwungenerUmbruch">${applicationScope['org.glassfish.jsp.isStandaloneWebapp']}</td></tr>
    <tr><td class="schluesselwert">\${fn:length(applicationScope.persons)}</td><td class="erzwungenerUmbruch">${fn:length(applicationScope.persons)}</td></tr>
        <c:forEach items="${applicationScope}" var="p">
        <tr>
            <td class="schluesselwert">${p.key}</td>
            <td class="erzwungenerUmbruch">
                <c:catch>
                    ${p.value}
                </c:catch>
                <c:if test = "${catchException != null}">
                    <p class="raginWarning">Got NPE : ${catchException} <br />
                        There is an exception: ${catchException.message}</p>
                    </c:if>

            </td>
        </tr>
    </c:forEach>
</table>

<h3><span class="label label-primary">Session attributes</span></h3>    
<table class="table table-striped ragintable" >
    <c:forEach items="${sessionScope}" var="p">
        <tr>
            <td class="schluesselwert">${p.key}</td>
            <td class="erzwungenerUmbruch">${p.value}</td>
        </tr>
    </c:forEach>
</table>

<h3><span class="label label-primary">Request attributes</span></h3>
<table class="table table-striped ragintable" >
    <c:forEach items="${requestScope}" var="p">
        <tr>
            <td class="schluesselwert">${p.key}</td>
            <td class="erzwungenerUmbruch">${p.value}</td>
        </tr>
    </c:forEach>
</table>

<h3><span class="label label-primary">Page attributes</span></h3>
<table class="table table-striped ragintable" >
    <c:forEach items="${pageScope}" var="p">
        <tr>
            <td class="schluesselwert">${p.key}</td>
            <td class="erzwungenerUmbruch">${p.value}</td>
        </tr>
    </c:forEach>
</table>


<a name="sysprop"></a>
<h3><span class="label label-primary">System properties</span></h3>

<%
    pageContext.setAttribute("props", System.getProperties());
    TreeMap tm = new TreeMap(System.getProperties());
    pageContext.setAttribute("propsSorted", tm);
%>

<table class="table table-striped ragintable" >
    <c:forEach items="${propsSorted}" var="entry">
        <tr>
            <td class="schluesselwert">${entry.key}</td>
            <td class="erzwungenerUmbruch">${entry.value}</td>
        </tr>
    </c:forEach>        
</table>

<a name="questions"></a>
<h3><span class="label label-primary">Questions</span></h3>

<form id="formQuestions">
    <c:forEach items="${questions}" var="eineFrage">
    <!------------------------------- Question ${eineFrage.ID} -------------------------------->
        <h3 id="questionID${eineFrage.ID}" >
            <span class="glyphicon glyphicon-edit" aria-hidden=true></span>
            <span class="label label-default">${eineFrage.text}</span></h3>
        <div class="input-group" id="selectID${eineFrage.ID}" >
            <span class="input-group-addon">
                <c:forEach items="${eineFrage.alAntworten}" begin="0" var="antwort" varStatus="count">
                    <div class="checkbox" align="left" >
                        <label><input id="selectID${eineFrage.ID}"  type="checkbox" value="${count.index +1}">${count.index +1}: ${antwort}</label></div>
                        </c:forEach> 
            </span>
        </div><!-- /input-group -->            
        <h5 id="answerID${eineFrage.ID}" style="display:display;">Korrekte Antwort: ${eineFrage.indexRichtigeAntwort +1} -> ${eineFrage.alAntworten[eineFrage.indexRichtigeAntwort  ]}</h5>

    </c:forEach>
</form>
<pre>${questions}</pre>
<h3><span class="label label-primary">Static resources within jar-files</span></h3>
<img src="static-a.jpg" width="100px" alt="static resource within jar a">
<img src="static-b.jpg" width="100px" alt="static resource within jar b">
<img src="static-c.jpg" width="100px" alt="static resource within jar c">
<rt:foot/>