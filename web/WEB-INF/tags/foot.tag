<!-- ------------------------------ foot.tag ----------------------------- -->
<%@ tag description="footer tag file" %>
<%@ attribute  name="foot" fragment="foot_frg"%>
<hr/>
<footer align="center">
    <p><span class="label label-success">Your IP is:<%=request.getRemoteAddr()%>!</span></p>
    
    <p>User: [${sessionScope.userid} ] |
    <a href="/"  target="_new"> Glassfisch </a>  | <a href="/RaginWA2/" > RaginWA2 </a> 
    | <a href="http://localhost:4848/common/index.jsf" target="_new"> Glassfisch Admin </a> 
    </p>
</footer>
    
<script type="text/javascript">
// create the back to top button
$('body').prepend('<a href="#" class="back-to-top">Back to Top</a>');

var amountScrolled = 300;

$(window).scroll(function() {
	if ( $(window).scrollTop() > amountScrolled ) {
		$('a.back-to-top').fadeIn('slow');
	} else {
		$('a.back-to-top').fadeOut('slow');
	}
});

$('a.back-to-top, a.simple-back-to-top').click(function() {
	$('html, body').animate({
		scrollTop: 0
	}, 700);
	return false;
});
</script>    
    
    
</body>
</html>
<!-- ------------------------------ foot.tag ----------------------------- -->