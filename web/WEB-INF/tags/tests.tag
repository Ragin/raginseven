

<%@ taglib prefix="ragin" uri="/org/ragin/seven" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="message"%>

<%-- any content can be specified here e.g.: --%>
<h2>${message}</h2>
<%-- setting the ccontext path for usage in any EL --%>
<c:set var="contextPath2" value="${pageContext.request.contextPath}" scope="application"/>       
<c:out value="${contextPath2}"/>
<br/>
<c:out value="${contextPath}"/>
<h4>Example: Reading context init param using EL</h4>
${initParam.appname}
<h4> Example: Using a Tag library function</h4>
<p>${ragin:identifyDemoPerson()}</p>
<h4>Example: Using a custom tag</h4>


<p>Hello 
    
<ragin:hello surname="Mueller" prename="Marc" >
        <jsp:attribute name="cityOfBirth">Wipperfürth</jsp:attribute>
</ragin:hello>!</p>

        <h4>Example: Using a tag file</h4>
<rt:demo pversion="${initParam.productversion}" entwickler="${initParam.developer}" platform="Windows"/> 

<h4>Example core standard Tags</h4>
<c:set var="color" scope="request">rot,gelb,blau</c:set>
    <select name="farbwahl">
    <c:forEach var="singleColor" items="${requestScope.color}">
        <option value="color_${singleColor}">${singleColor}</option>
    </c:forEach>
</select>