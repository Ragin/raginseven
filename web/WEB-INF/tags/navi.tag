<!-- ------------------------------ navi.tag ----------------------------- -->


<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="message"%>

<%-- any content can be specified here e.g.: --%>
<nav class="navbar navbar-default">

    <div class="container">
        <div class="navbar-header" style="margin: auto">

            <a class="navbar-brand"  href="index">

                ${initParam.appname}[<c:out value="${sessionScope['loginUser']}" />]
            </a> 
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" 
                       role="button" aria-haspopup="true" aria-expanded="false">
                        Exam<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="lform">Login</a></li>
                        <li><a href="examNew">New Test</a></li>
                        <li><a href="logout">Logout</a></li>
                    </ul>
                </li>


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" 
                       data-toggle="dropdown" role="button" aria-haspopup="true" 
                       aria-expanded="false">Infos<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="showSystem">All</a></li>
                        <li><a href="showSystem#log4j">Info Log4J Configuration</a></li>
                        <li><a href="showSystem#attributes">Info Attributes</a></li>
                        <li><a href="showSystem#params">Info Parameters</a></li>
                        <li><a href="showSystem#sysprop">System Properties</a></li>
                        <li><a href="showSystem#questions">Info Questions</a></li>
                        <li><a href="questionsShowDatabase">Show questions database</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" 
                       role="button" aria-haspopup="true" aria-expanded="false">
                        Person<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="newPerson">New person</a></li>
                        <li><a href="showPerson">Show person</a></li>
                        <li><a href="listPersons">List persons</a></li>
                    </ul>
                </li>


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" 
                       role="button" aria-haspopup="true" aria-expanded="false">
                        Demos<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="welcomePageWithExamples?age=50">Welcome page with examples</a></li>
                        <li><a href="404-not-found">404 error</a></li>
                        <li><a href="reg">Reqister</a></li>
                        <li><a href="upload">Upload file</a></li>
                        <li><a href="logout">logout</a></li>
                        <li><a href="ProgressServlet">ProgressServlet</a></li>
                        <li><a href="ProgressAsyncServlet">ProgressAsyncServlet</a></li>
                        <li><a href="reg">reg</a></li>
                        <li><a href="welcome">welcome</a></li>
                        <li><a href="static-a.jpg">Link static resource a withín jar file</a></li>
                        <li><a href="static-b.jpg">Link static resource b withín jar file</a></li>
                        <li><a href="#">-------</a></li>
                        <li><a href="text">Demo UpperCaseFilter</a></li>
                        <li><a href="LongRunningServlet?time=2000">LongRunningServlet 2000 ms</a></li>
                        <li><a href="LongRunningServlet?time=4000">LongRunningServlet 4000 ms</a></li>
                        <li><a href="RSecurity">RSecurity Servlet Demo Chef / Angestellter</a></li>
                        
                    </ul>
                </li>                
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" 
                       role="button" aria-haspopup="true" aria-expanded="false">
                        Infos<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="appLog">Application Log </a></li>
                        <li><a href="clearAppLog">Clear Application Log </a></li>
                        <li><a href="serverLog">Server Log</a></li>
                        <li><a href="clearServerLog">Clear Server Log (not implemented)</a></li>
                        <li><a href="/" target="_blank">Glassfish </a> </li>
                        <li><a href="http://localhost:4848/" target="_blank">
                                Glassfish Console</a></li>
                    </ul>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav> 
<!-- ------------------------------ navi.tag ----------------------------- -->                    