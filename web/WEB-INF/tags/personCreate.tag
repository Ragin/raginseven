<!-- ------------------------------ personCreate.tag ----------------------------- -->
<%@tag description="form for new person" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="message"%>

<%-- any content can be specified here e.g.: --%>


<h3><span class="label label-primary">New person</span></h3>

<form class="form-horizontal" target="" method="POST" action="PersonController">
<fieldset>

<!-- Form Name -->
<legend>Enter new Person:</legend>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Title">Title</label>
  <div class="col-md-4">
    <select id="Title" name="Title" class="form-control">
      <option value="Herr">Herr</option>
      <option value="Frau">Frau</option>
      <option value="Dr.">Dr.</option>
      <option value="Prof.">Prof.</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Prename">Prename</label>  
  <div class="col-md-5">
      <input id="Prename" name="Prename" type="text" placeholder="Prename" class="form-control input-md" required="" value="Susanne">
  <span class="help-block">Enter the prename</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="MiddleName">Middle Name</label>  
  <div class="col-md-5">
  <input id="MiddleName" name="MiddleName" type="text" placeholder="MiddleName" class="form-control input-md"  value="A.">
  <span class="help-block">Enter the middle name</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="LastName">Last Name</label>  
  <div class="col-md-5">
  <input id="LastName" name="LastName" type="text" placeholder="LastName" class="form-control input-md" required=""  value="Plast">
  <span class="help-block">Enter the last name</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Address">Address</label>  
  <div class="col-md-4">
  <input id="Address" name="Address" type="text" placeholder="Address" class="form-control input-md" value="Gehweg 34">
  <span class="help-block">Enter the street</span>  
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="ZIP">PLZ</label>  
  <div class="col-md-5">
  <input id="ZIP" name="ZIP" type="text" placeholder="ZIP" class="form-control input-md" required=""  value="58455">
  <span class="help-block">Enter ZIP code</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="City">City</label>  
  <div class="col-md-5">
  <input id="City" name="City" type="text" placeholder="City" class="form-control input-md" required=""  value="Berlin">
  <span class="help-block">Enter the city</span>  
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Country">Country</label>
  <div class="col-md-5">
    <select id="Country" name="Country" class="form-control">
      <option value="DE">Deutschland</option>
      <option value="INTERNATIONAL">International</option>
    </select>
      <span class="help-block">Enter the country</span>  
  </div>
</div>


<!-- Text input-->
<div class="form-group">
    <!--
  <label class="col-md-4 control-label" for="DayOfBirth">Day Of Birth</label>  
  <div class="col-md-5">
  <input id="DayOfBirth" name="DayOfBirth" type="text" placeholder="Day Of Birth" class="form-control input-md" value="09.12.1984">
  <span class="help-block">Enter the day of birth</span>  
  </div>
  -->
  <label class="col-md-4 control-label"  for="DayOfBirth">Day of Birth</label>
  <div class="col-md-5">
  <input id="DayOfBirth"  name="DayOfBirth" type="date" value="2016-01-13">
  <span class="help-block">Enter the day of birth</span>  
  </div>
      
  
  
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="COB">City of birth</label>  
  <div class="col-md-5">
  <input id="COB" name="COB" type="text" placeholder="COB" class="form-control input-md" required=""  value="Wuppertal">
  <span class="help-block">Enter the city of birth</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Phone">Phone</label>  
  <div class="col-md-5">
  <input id="Phone" name="Phone" type="text" placeholder="Phone" class="form-control input-md" value="0202-454545">
  <span class="help-block">Enter the phone number</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="MobilePhone">MobilePhone</label>  
  <div class="col-md-5">
  <input id="MobilePhone" name="MobilePhone" type="text" placeholder="MobilePhone" class="form-control input-md" value="01525-45459955">
  <span class="help-block">Enter the mobile phone number</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Email">Email</label>  
  <div class="col-md-5">
  <input id="Email" name="Email" type="text" placeholder="Email" class="form-control input-md" required="" value="susanne.plast@test.org">
  <span class="help-block">Enter the email address</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Website">Website</label>  
  <div class="col-md-5">
  <input id="Website" name="Website" type="text" placeholder="Website" class="form-control input-md" value="www.test.org">
  <span class="help-block">Enter the website</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="ImageName">ImageName</label>  
  <div class="col-md-5">
  <input id="ImageName" name="ImageName" type="text" placeholder="ImageName" class="form-control input-md"  value="susanne.a.plast.jpg">
  <span class="help-block">Enter the name of the image file with suffix</span>  
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Comment">Comment</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="Comment" name="Comment">Enter any comment</textarea>
  </div>  
</div>


<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="imagefilebutton">Image File</label>
  <div class="col-md-4">
    <input id="filebutton" name="imagefilebutton" class="input-file" type="file">
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submitbutton">Submit</label>
  <div class="col-md-4">
    <button id="submitbutton" name="submitbutton" class="btn btn-primary">Submit</button>
  </div>
</div>

</fieldset>
</form>
<!-- ------------------------------ personCreate.tag ----------------------------- -->