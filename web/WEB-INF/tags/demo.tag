<%@ tag description="einfaches demo fragment" %>
<%@ attribute  name="demo" fragment="demo_frg"%>

<%@ attribute name="entwickler" required="true" rtexprvalue="true" %>
<%@ attribute name="pversion" required="true" rtexprvalue="true" %>
<%@ attribute name="platform" required="false" rtexprvalue="false" %>
<p>Dieser Text wird durch ein einfaches Tag file eingebunden. Der Entwickler war
    '${entwickler}' und die Version der Anwendung ist: ${pversion} .</p>
<p>Zielplattform ist: ${platform}</p>
